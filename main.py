from utils import str_to_dict

if __name__ == "__main__":
    languages = ["Python", "JavaScript", "Ruby", "PHP", "Java"]
    for language in languages:
        print(str_to_dict(language))

    # Ternary operator
    x = 10
    y = 10
    print(
        "Game over" if x == 0 and y == 0 else x + y if x < y else x - y if x > y else 0
    )
