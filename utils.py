def get_letter_probability(letter: str, string: str) -> float:
    # Calculates the probability as a percentage
    return round(string.count(letter) * 100 / len(string), 1)


def str_to_dict(string: str, case_sensitive: bool = False) -> dict:
    # Converts a string into a dictionary

    if case_sensitive:
        string = string.lower()

    probability = list()
    letters = set(string)
    for letter in letters:
        probability.append(get_letter_probability(letter, string))

    # Returns dictionary where the key is a letter,
    # the value is the percentage probability of finding this letter in the string
    return dict(zip(letters, probability))
